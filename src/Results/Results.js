import React, { Component } from 'react';
import { Button, List, ListItem, ListItemText } from '@material-ui/core'
import foursquare from '../Foursquare/api';
import './Results.css'

class Results extends Component {
	constructor(props) {
		super(props);
		this.state = {
			...props.location.state,
			placesCount: 0
		};

		if(!this.state.location && !this.state.eventType && !this.state.distance) {
			this.reset();
		}
	}

	componentDidMount() {
		foursquare.searchVenues(this.state)
			.then(response => {
				const items = response.response.groups[0].items;
				this.setState({
					places: items,
					placesCount: items.length
				})
			})
	}

	reset = () => {
		this.props.history.push('/');
	};

	render() {
		return (
			<div className="results">
				<span className="results-title">Places({this.state.placesCount})</span>
				<List className="results-list" component="nav">
					{this.state.places &&
					this.state.places.map((place, index) =>
						<ListItem key={index}>
							<ListItemText primary={place.venue.name} secondary={place.venue.location.address} />
						</ListItem>

					)
					}
				</List>
				<Button variant="contained"  color="secondary" onClick={this.reset}>Reset search</Button>
			</div>
		);
	}
}

export default Results;