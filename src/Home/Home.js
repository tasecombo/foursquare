import React, { Component } from 'react';
import './Home.css';
import Geolocation from 'react-geolocation';

import { Button } from '@material-ui/core';

class Home extends Component {
	setLocation = location => {
		if(location) {
			this.setState({
				location: {
					longitude: location.coords.longitude,
					latitude: location.coords.latitude
				}
			})
		}
	};

	redirectToConfigure = () => {
		this.props.history.push('/configure', { location: this.state.location });
	};

	render() {
		return (
			<div className="home">
				<span className="title">Find a place to hang out with your friends!</span>
				<span className="locationLabel">Please enable your location in order to continue...</span>
				<Geolocation onSuccess={this.setLocation.bind(this)}
					render={({
								 fetchingPosition,
								 error,
								 getCurrentPosition
							 }) =>
						<div className="get-location">
							<Button variant="outlined"  color="secondary" onClick={getCurrentPosition}>Get Location</Button>
							{error &&
							<div>
								{error.message}
							</div>}
						</div>}
				/>

				{this.state.location &&
				<Button variant="contained" color="primary" onClick={this.redirectToConfigure}>Continue</Button>
				}
			</div>
		);
	}
}

export default Home;
