import React, { Component } from 'react';
import './App.css';
import Home from './Home/Home'
import Configurator from './Configurator/Configurator';
import Results from './Results/Results';
import { BrowserRouter, Route } from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <div className="App">
          <BrowserRouter >
              <div>
            <Route exact path={"/"} component={Home}/>
            <Route exact path={"/configure"} component={Configurator}/>
            <Route exact path={"/results"} component={Results}/>
              </div>
          </BrowserRouter>
      </div>
    );
  }
}

export default App;
