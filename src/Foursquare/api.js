const FOURSQUARE_API_PATH = 'https://api.foursquare.com/v2/';
const client = {
	client_id: '',
	client_secret: ''
};
const version = '20181101';

export default {
	searchVenues: parameters => {
		const url = new URL(`${FOURSQUARE_API_PATH}venues/explore`);
		const params =  {
				...client,
				ll: `${parameters.location.latitude}, ${parameters.location.longitude}`,
				radius: parameters.distance,
				query: parameters.eventType,
				v: version,
			};

		Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

		return fetch(url)
			.then((res) => res.json());
	}
}
