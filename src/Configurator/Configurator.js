import React, { Component } from 'react';
import {
	Button,
	List,
	ListItem,
	ListItemIcon,
	ListItemText
} from '@material-ui/core';
import { Slider } from '@material-ui/lab';
import './Configurator.css';

const events = {
	0: 'restaurants',
	1: 'bars',
	2: 'clubs'
};

const eventTypes = [
	{
		text: 'Food',
		icon: 'Restaurant'
	},
	{
		text: 'Drinks',
		icon: 'LocalBar'
	},
	{
		text: 'Nightlife',
		icon: 'PartyMode'
	}
];

class Configurator extends Component {
	constructor(props){
		super(props);
		this.state = {
			...props.location.state,
			distance: 1000
		};
	}
	continue = () => {
		this.props.history.push('/results', { ...this.state});
	};

	handleChange = (event, value) => {
		this.setState({distance: value});
	};

	handleListItemClick = (event, value) => {
		this.setState({
			eventType: events[value],
			selectedIndex: value
		})
	};
	materialIcon = ({ icon }) => {
		let resolved = require(`@material-ui/icons/${icon}`).default;
		if (!resolved) {
			throw Error(`Could not find icon ${icon}`)
		}

		return React.createElement(resolved)
	};

	render() {
		const { distance } = this.state;

		return (
			<div className="configurator">
				<div className="configurator-distance">
					<span className="configurator-title">Distance range: {Math.round(distance, 0)} m</span>
					<Slider
						value={distance}
						min={1} max={2000}
						onChange={this.handleChange}
						className="configurator-distance-slider"
					/>
				</div>
				<div className="configurator-events">
					<span className="configurator-title">Choose the kind of place you wanna go:</span>
					<List className="configurator-events-list" component="nav">
						{eventTypes.map((item, index) =>
						<ListItem
							button
							selected={this.state.selectedIndex === index}
							onClick={event => this.handleListItemClick(event, index)}
							key={index}
						>
							<ListItemIcon>
								{this.materialIcon(item)}
							</ListItemIcon>
							<ListItemText>{item.text}</ListItemText>
						</ListItem>
						)}
					</List>
				</div>
				{this.state.eventType &&
				<Button variant="contained" color="primary" onClick={this.continue.bind(this)}>Search</Button>
				}
			</div>
		);
	}
}

export default Configurator;
